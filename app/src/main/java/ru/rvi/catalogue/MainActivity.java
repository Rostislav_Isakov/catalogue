package ru.rvi.catalogue;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import static ru.rvi.catalogue.R.layout.activity_main;

public class MainActivity extends AppCompatActivity implements IConstants, ListView.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_main);
        setMainList();
    }

    /**
     * Метод, который устанавливает на главный экран
     * список типов звезд
     */
    private void setMainList() {
        String[] typesOfStars = getResources().getStringArray(R.array.types_of_stars);
        ListView listView = findViewById(R.id.list_view);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.list_view_item, typesOfStars);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String[] fileNames = getResources().getStringArray(R.array.file_names);
        Intent intent = new Intent(MainActivity.this, IntentActivity.class);
        intent.putExtra(INTENT_KEY, fileNames[position]);
        startActivity(intent);
    }
}
