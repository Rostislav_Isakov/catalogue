package ru.rvi.catalogue;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import static ru.rvi.catalogue.R.layout.activity_intent;

import androidx.appcompat.app.AppCompatActivity;

public class IntentActivity extends AppCompatActivity implements IConstants {

    @Override
    protected void onCreate(Bundle savedBundle) {
        super.onCreate(savedBundle);
        setContentView(activity_intent);
        setShownContent();
    }

    private void setShownContent() {
        WebView webView = findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        Intent intent = getIntent();
        String fileName = intent.getStringExtra(INTENT_KEY);
        webView.loadUrl(ANDROID_URL + fileName);
    }
}
